#!/usr/bin/env python3.11

# Copyright (C) 2023-2024 Collabora Ltd
#
# Permission is hereby granted, free of charge, to any person obtaining a
# copy of this software and associated documentation files (the "Software"),
# to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense,
# and/or sell copies of the Software, and to permit persons to whom the
# Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included
# in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
# OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
# THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR
# OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
# ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
# OTHER DEALINGS IN THE SOFTWARE.
#
# SPDX-License-Identifier: MIT

__author__ = "Sergi Blanch Torne"
__email__ = "sergi.blanch.torne@collabora.com"
__copyright__ = "Copyright (C) 2023-2024 Collabora Ltd"


from .abstract import Singleton
import colorlog
import logging
from logging import INFO, DEBUG
from pathlib import Path
import sys
import traceback
from typing import Callable


class UprevLogger(Singleton):
    __logger: logging.Logger = None

    def __init__(self):
        if not self.__logger:
            self.__logger = colorlog.getLogger("ci-uprev")
            self.__configure_logging()

    @property
    def logger_obj(self) -> logging.Logger:
        return self.__logger

    def critical(self, *args, **kwargs) -> None:
        self.__do_log(self.__logger.critical, *args, **kwargs)

    def error(self, *args, **kwargs) -> None:
        self.__do_log(self.__logger.error, *args, **kwargs)

    def warning(self, *args, **kwargs) -> None:
        self.__do_log(self.__logger.warning, *args, **kwargs)

    def info(self, *args, **kwargs) -> None:
        self.__do_log(self.__logger.info, *args, **kwargs)

    def debug(self, *args, **kwargs) -> None:
        self.__do_log(self.__logger.debug, *args, **kwargs)

    def __do_log(self, printer_method: Callable, *args, **kwargs) -> None:
        try:
            printer_method(*args, **kwargs)
        except Exception:
            self.__logger.error(traceback.format_exc())

    def __configure_logging(self):
        file_handler = logging.FileHandler(filename=self.__logfilename())
        console_handler = logging.StreamHandler(stream=sys.stdout)

        file_handler.formatter = logging.Formatter(
            "%(asctime)s - %(levelname)s - %(threadName)s - %(message)s"
        )
        console_handler.formatter = colorlog.ColoredFormatter(
            "%(log_color)s%(message)s",
            log_colors={
                "DEBUG": "blue",
                "INFO": "white",
                "WARNING": "yellow",
                "ERROR": "red",
                "critical": "red,bg_white",
            },
        )

        self.__logger.setLevel(DEBUG)
        file_handler.level = DEBUG
        console_handler.level = INFO

        self.__logger.addHandler(file_handler)
        self.__logger.addHandler(console_handler)

    @staticmethod
    def __logfilename() -> str:
        results = Path("./results")
        if not results.exists():
            results.mkdir()
        if results.is_dir():
            file = Path(results, "ci-uprev.log")
        else:
            file = Path("ci-uprev.log")
        return "/".join(file.parts)

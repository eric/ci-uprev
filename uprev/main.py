#!/usr/bin/env python3.11

# Copyright (C) 2022-2024 Collabora Ltd
#
# Permission is hereby granted, free of charge, to any person obtaining a
# copy of this software and associated documentation files (the "Software"),
# to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense,
# and/or sell copies of the Software, and to permit persons to whom the
# Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included
# in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
# OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
# THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR
# OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
# ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
# OTHER DEALINGS IN THE SOFTWARE.
#
# SPDX-License-Identifier: MIT

__author__ = "Tomeu Vizoso and Sergi Blanch Torne"
__email__ = "sergi.blanch.torne@collabora.com"
__copyright__ = "Copyright (C) 2022-2024 Collabora Ltd"

from .abstract import JobStageGroup
from argparse import ArgumentParser, Namespace
import base64
from collections import defaultdict, deque, OrderedDict
from concurrent.futures import ThreadPoolExecutor
from concurrent.futures import wait as wait_threads_pool
from copy import deepcopy
from datetime import datetime
from .environment import (
    GitlabProxy,
    Target,
    Fork,
    get_target_project,
    Dependency,
    Working,
    Template,
    UpdateRevisionPair,
    LocalClone,
    in_production,
    Branch,
    Title,
)
from .exceptions import NothingToUprev
from .expectations import (mesa_expectations_paths,
                           virglrenderer_expectations_paths)
from functools import cache
import git
from gitlab.base import RESTObjectList
from gitlab.exceptions import GitlabGetError, GitlabUpdateError
from gitlab.v4.objects import (
    Project,
    ProjectBranch,
    ProjectCommit,
    ProjectIssue,
    ProjectJob,
    ProjectMergeRequest,
    ProjectNote,
    ProjectPipeline,
    ProjectPipelineJob,
)
from .logging import UprevLogger
from logging import ERROR
import os
from pprint import pformat
import re
from shutil import rmtree
import sys
from tempfile import TemporaryDirectory
from tenacity import retry, stop_after_attempt, wait_exponential, after_log
import time
from typing import Optional, Tuple, Union
from .uprevs import (
    UpdateRevision,
    UprevActionProcessor,
    uprev_mesa_in_virglrenderer,
    uprev_piglit_in_mesa,
)
from zipfile import ZipFile


# GLOBAL constants

LINES_PER_CATEGORY_TO_SUMMARY_IN_NOTE: int = 10
MAX_LINES_PER_CATEGORY_IN_NOTE: int = 100

CHECKER_SLEEP_TIME: int = 10  # seconds
GL_OBJ_CREATION_TIMEOUT: int = 600  # seconds, 10 minutes


def defaultdictdeque() -> defaultdict:
    return defaultdict(deque)


def get_candidate_for_uprev() -> Tuple[int, str]:
    """
    First get pipelines in the project where it has to update the revision.
    Then, chose the first one that satisfy specific conditions.
    :return: pipeline id and commit hash
    """
    pipelines = __get_pipelines()
    pipeline = __select_pipeline(pipelines)
    if not pipeline:
        raise LookupError("No uprev candidate found")
    return pipeline.id, pipeline.sha


def __get_pipelines(
    status: str = "success", username: str = "marge-bot"
) -> RESTObjectList:
    """
    To update the revision, the first requisite is a pipeline that:
    1. It has to have succeeded,
    2. It has to be merged by marge
    :return: iterator over pipelines
    """
    dependency_project = Dependency().gl_project
    return dependency_project.pipelines.list(
        status=status,
        username=username,
        ordered_by="created_at",
        sort="desc",
        iterator=True,
    )


def __select_pipeline(pipelines: RESTObjectList) -> ProjectPipeline:
    """
    Specific condition that the pipeline candidate must satisfy. In the case of
    mesa for virglrenderer it is necessary (because it uses some artifacts from
    the merge pipeline) to have the 'debian-testing' job.
    :return: the pipeline to use in the uprev.
    """
    uprev_pair = UpdateRevisionPair().enum
    for pipeline in pipelines:
        if uprev_pair == UpdateRevision.mesa_in_virglrenderer:
            debian_testing_job = [
                job
                for job in pipeline.jobs.list(iterator=True)
                if job.name == "debian-testing"
            ]
            if debian_testing_job:
                return pipeline
        else:
            return pipeline


def get_templates_commit(
        revision: str,
        filename: str = ".gitlab-ci.yml"
) -> str:
    """
    Find in a file of a gitlab project the definition of the
    MESA_TEMPLATES_COMMIT hash to be used.
    :param revision: define with commit, tag, branch use to read the file
    :param filename: file of the project where the variable is defined
    :return: hash of ci-templates to use in the uprev
    """
    template_project = Template().gl_project
    file = template_project.files.get(filename, ref=revision)
    file_content = base64.b64decode(file.content).decode("utf-8")
    for line in file_content.split("\n"):
        # sample line '    MESA_TEMPLATES_COMMIT: &ci-templates-commit <hash>'
        # and we are interested in the hash at the end
        pattern = r"MESA_TEMPLATES_COMMIT:.* ([0-9a-fA-F].*)$"
        if search_result := re.search(pattern, line):
            return search_result.group(1)


def create_branch(
    pipeline_id: int,
    revision: str,
    templates_commit: str,
) -> git.Repo:
    """
    With the uprev information, clone locally the target project and prepare
    the temporal working branch from the target's default branch. Then do the
    uprev itself and commit it.
    :param pipeline_id: reference pipeline of the dep project to uprev
    :param revision: dep project revision to uprev
    :param templates_commit: ci-templates commit to uprev
    :return: git repository object
    """
    repo = __clone_repo(force_fresh_repo=False)
    __clean_previous_tmp_branch()

    os.chdir(repo.working_dir)
    try:
        amend, revision_in_use = __checkout_wip_branch(repo)
        __do_uprev(
            repo,
            pipeline_id,
            revision,
            templates_commit,
            amend_previous_uprev=amend,
            inject_old_revision=revision_in_use,
        )
    finally:
        os.chdir("..")

    return repo


def __clone_repo(
    force_fresh_repo: bool,
) -> git.repo:
    """
    Prepare a local clone of the repository with the target project and its
    fork. It can reuse or clean a previous clone. And in case of clone from
    scratch, setup the git config.
    :param force_fresh_repo: flag what to do if the local clone already exists
    :return:
    """
    local_clone = LocalClone().directory
    target_project = get_target_project().gl_project
    if force_fresh_repo:
        if os.path.exists(local_clone):
            UprevLogger().warning(
                f"Remove previously existing repo in {local_clone}"
            )
            rmtree(local_clone)
    if os.path.exists(local_clone):
        UprevLogger().warning(f"Reusing repo in {local_clone}")
        repo = git.Repo(local_clone)
        repo.remote("origin").update()
        repo.remote("fork").update()

        tmp_branch = Branch().for_wip
        if tmp_branch in repo.heads:
            UprevLogger().warning("Deleting existing branch")
            repo.heads[target_project.default_branch].checkout(force=True)
            repo.delete_head(tmp_branch, force=True)
    else:
        username = GitlabProxy().gl_obj.user.username
        useremail = GitlabProxy().gl_obj.user.email
        token = GitlabProxy().token
        project_path = get_target_project().path_with_namespace
        fork_path = Fork().path_with_namespace

        UprevLogger().info(f"Cloning repo at {local_clone}")
        repo_url = f"https://gitlab.freedesktop.org/{project_path}.git"
        repo = git.Repo.clone_from(
            repo_url, local_clone, branch=target_project.default_branch
        )
        repo.config_writer().set_value(
            "user", "name", "Collabora's Gfx CI Team"
        ).release()
        repo.config_writer().set_value("user", "email", useremail).release()
        fork_url = \
            (f"https://{username}:{token}@"
             f"gitlab.freedesktop.org/{fork_path}.git")
        repo.create_remote("fork", url=fork_url)
        repo.remotes["fork"].fetch()
    return repo


def __clean_previous_tmp_branch() -> None:
    """
    The temporary branch could still be in the fork repo because there is
    another uprev procedure in progress or because one failed lefting it
    orphan. So, when it is orphan a newer uprev procedure could fail and
    requires it to be cleaned.
    :return:
    """
    uprev_tmp_branch = Branch().for_wip
    if (branch := __get_branch(uprev_tmp_branch)) is None:
        return
    UprevLogger().warning(f"The branch {uprev_tmp_branch} exists in the fork")
    __wait_for_running_pipelines_in_branch(branch)
    UprevLogger().warning(f"Remove orphan {uprev_tmp_branch} branch")
    branch.delete()


def __checkout_wip_branch(repo: git.repo) -> Tuple[bool, str]:
    """
    This method points the git clone to the Work In Progress (WIP) branch to
    prepare an uprev candidate. If there is a previous uprev proposal, it takes
    the branch to start the new proposal from here and rebase.
    It returns a boolean to report if the new uprev has to amend the last
    commit or it has to create a fresh one.
    :param repo: the object managing the git clone
    :return: if the WIP branch already has the uprev commit
    """
    target_project = get_target_project().gl_project
    fork_project = Fork().gl_project
    merge_request = __get_uprev_merge_request(Title().for_merge_request)
    if merge_request is not None:
        UprevLogger().info(
            f"There is a previous uprev proposal (merge request "
            f"{merge_request.web_url}) with branch "
            f"{merge_request.source_branch}, so the current uprev attempt "
            f"will start from rebasing it."
        )
        old_revision = __check_dependency_revision(repo)
        if in_production():
            repo.git.checkout(merge_request.source_branch)
        else:
            repo.git.checkout("-t", f"origin/{merge_request.source_branch}")
        repo.git.checkout("-b", Branch().for_wip)
        try:
            repo.git.rebase(target_project.default_branch)
        except git.exc.GitCommandError:
            repo.git.rebase("--abort")
            UprevLogger().warning(
                f"Rebase {merge_request.source_branch} (aka theirs) with "
                f"{target_project.default_branch} (aka ours) failed. "
                f"Retry rebase with strategy option 'ours' to prevail what's "
                f"in the project."
            )
            repo.git.rebase(
                target_project.default_branch,
                "--strategy=recursive",
                "--strategy-option=ours",
            )
            push_to_the_fork(repo, wait_pipeline_creation=False)
            UprevLogger().info(
                f"Start with the rebased "
                f"(with {target_project.default_branch} branch) "
                f"commit with SHA {repo.head.commit.hexsha} "
                f"("
                f"{fork_project.commits.get(repo.head.commit.hexsha).web_url}"
                f")."
            )
        return True, old_revision
    else:
        # if there isn't a previous uprev proposal, the wip branch starts from
        # the default branch
        repo.heads[target_project.default_branch].checkout(
            force=True, b=Branch().for_wip
        )
        UprevLogger().info(
            f"Start working from the commit with SHA "
            f"{repo.head.commit.hexsha} "
            f"({target_project.commits.get(repo.head.commit.hexsha).web_url})"
        )
        return False, ""


def __check_dependency_revision(repo: git.Repo) -> str:
    """
    From a clone from the default branch, read which is the dependency's
    revision in use in the project, to store it for later use.
    The main purpose is, then it is continuing a previous proposal, inject the
    old revision to build correctly the diff_url.
    :param repo: the object managing the git clone
    :return: the revision of the dependency in the default branch
    """
    UprevLogger().debug(
        f"Dry run an uprev using the information in "
        f"{get_target_project().gl_project.default_branch}"
    )
    uprev_obj = __do_uprev(repo, dry_run=True)
    return uprev_obj.old_revision


def __do_uprev(
    repo: git.Repo,
    pipeline_id: int = None,
    revision: str = None,
    templates_commit: str = None,
    amend_previous_uprev: bool = False,
    dry_run: bool = False,
    inject_old_revision: str = None,
) -> UprevActionProcessor:
    """
    Given all the necessary data, proceed with the uprev core procedure.
    :param repo: the object managing the git clone
    :param pipeline_id: pipeline id from where the uprev candidate
           were selected
    :param revision: uprev candidate revision
    :param templates_commit: revision of the templates project
    :param amend_previous_uprev: if there is a previous uprev to continue
    :param dry_run: flag to avoid to write in the local clone
    :param inject_old_revision: original revision in default branch to replace
           the value when continue a previous attempt
    :return: Object representing and managing the uprev core procedure
    """
    uprev_pair = UpdateRevisionPair().enum
    if uprev_pair == UpdateRevision.mesa_in_virglrenderer:
        uprev_obj = uprev_mesa_in_virglrenderer(
            repo,
            pipeline_id,
            revision,
            templates_commit,
            amend=amend_previous_uprev,
            inject_old_revision=inject_old_revision,
            dry_run=dry_run,
        )
    elif uprev_pair == UpdateRevision.piglit_in_mesa:
        uprev_obj = uprev_piglit_in_mesa(
            repo,
            revision,
            amend=amend_previous_uprev,
            inject_old_revision=inject_old_revision,
            dry_run=dry_run,
        )
    else:
        raise NotImplementedError(
            f"Not yet implemented the uprev of " f"{uprev_pair.name}"
        )
    Dependency().diff = uprev_obj.diff_url
    if not dry_run:
        UprevLogger().info(
            f"Created commit with SHA {repo.head.commit.hexsha}. Changes in "
            f"the dependency {uprev_obj.diff_url}"
        )
    return uprev_obj


def __get_branch(name: str) -> Optional[ProjectBranch]:
    """
    Get the branch gitlab object or return None hiding any exception
    :param name: string with branch name
    :return: The project branch object or None
    """
    try:
        fork_project = Fork().gl_project
        return fork_project.branches.get(name)
    except GitlabGetError:
        return


def __wait_for_running_pipelines_in_branch(branch: ProjectBranch) -> None:
    """
    When a branch have pipelines running, wait until they finish. They could be
    part of a procedure that could fail if we intervene.
    :param branch: branch that could have pipelines running.
    :return:
    """
    fork_project = Fork().gl_project
    uprev_tmp_branch_name = Branch().for_wip
    while active_pipelines := [
        pipeline.id
        for pipeline in fork_project.pipelines.list(
            sha=branch.commit["id"], iterator=True
        )
        if pipeline.status == "running"
    ]:
        if len(active_pipelines) > 0:
            UprevLogger().warning(
                f"Branch {uprev_tmp_branch_name} has {len(active_pipelines)} "
                f"pipeline(s) running. Waiting them to finish... "
                f"({active_pipelines})"
            )
            time.sleep(CHECKER_SLEEP_TIME)


def push_to_the_fork(
    repo: git.Repo, wait_pipeline_creation: bool = True
) -> Optional[ProjectPipeline]:
    """
    Push the commits in the local repository to the fork and wait until the
    last commit has a pipeline created (if not explicitly skipped).
    :param repo: local clone
    :param wait_pipeline_creation: boolean to skip the pipeline creation check
    :return: commit pipeline
    """
    remote = repo.remote("fork")
    UprevLogger().info("Push to the fork.")
    remote.push(force=True)
    UprevLogger().info(f"Repository head sha {repo.head.commit.hexsha}")
    Working().project = Fork()
    if wait_pipeline_creation:
        return __wait_pipeline_creation(repo.head.commit.hexsha)


def __wait_pipeline_creation(
        commit_hash: str,
        source: str = None
) -> ProjectPipeline:
    """
    For a pushed commit to the fork, Peaceful wait for the pipeline creation.
    We can use the 'source' parameter to fine-tune the pipeline wanted.
    :param commit_hash: string
    :param source: string
    :return: pipeline
    """
    UprevLogger().info(
        f"Waiting for pipeline to be created for {commit_hash} "
        f"{__get_commit(commit_hash).web_url}"
    )
    t_0 = datetime.now()
    query = {"sha": commit_hash}
    if source:
        query["source"] = source
    while True:
        pipeline = __query_pipeline(query)
        if pipeline:
            return pipeline
        # when not found
        if (datetime.now() - t_0).total_seconds() >= GL_OBJ_CREATION_TIMEOUT:
            raise RuntimeWarning(
                f"Timeout reached while waiting for pipeline creation for "
                f"commit {commit_hash}"
            )
        time.sleep(CHECKER_SLEEP_TIME)


@retry(
    stop=stop_after_attempt(10),
    wait=wait_exponential(multiplier=1, max=180),
    after=after_log(UprevLogger().logger_obj, ERROR),
)
# retry waiting 1, 2, 4, 8, 16, 32, 64, 128, 180... > 10 minutes
def __get_commit(commit_hash: str) -> ProjectCommit:
    """
    Get a commit object from the working project. If there is an exception, log
    it and reraise. It has also a decorator to use tenacity to retry.
    :param commit_hash: identifier string of the commit1
    :return: object in gitlab representing a commit.
    """
    try:
        working_project = Working().gl_project
        return working_project.commits.get(commit_hash)
    except Exception as exception:
        UprevLogger().error(
            f"Couldn't get the commit {commit_hash} "
            f"due to {type(exception)}: {exception}"
        )
        raise exception


@retry(
    stop=stop_after_attempt(10),
    wait=wait_exponential(multiplier=1, max=180),
    after=after_log(UprevLogger().logger_obj, ERROR),
)
def __query_pipeline(query: dict) -> Optional[ProjectPipeline]:
    """
    Get the first pipeline in a list generated by a query. When the list is
    empty, return None. But if there is any other exception, it uses tenacity's
    retry.
    :param query: dictionary as the query_parameters in GL api
    :return: the pipeline or None
    """
    try:
        project = Working().gl_project
        pipeline = next(
            project.pipelines.list(query_parameters=query, iterator=True)
        )
        return project.pipelines.get(pipeline.id)
    except StopIteration:
        return None


@retry(
    stop=stop_after_attempt(10),
    wait=wait_exponential(multiplier=1, max=180),
    after=after_log(UprevLogger().logger_obj, ERROR),
)
def __get_pipeline(idn: int, project: Project = None) -> ProjectPipeline:
    if project is None:
        project = Working().gl_project
    return project.pipelines.get(idn)


def run_pipeline(pipeline: ProjectPipeline) -> dict:
    """
    Trigger the manual jobs in the pipeline (waiting, if necessary, to the
    complete creation of the pipeline in the server) and wait until the
    pipeline finishes.
    Then collect the artifacts. Give it a second chance if any jobs still need
    to produce artifacts. And also, run a second time the ones that made
    failures artifacts.
    :param pipeline: git lab object
    :return: failures dict
    """
    UprevLogger().info(
        f"Waiting for pipeline to be ready {pipeline.web_url} "
        f"({pipeline.status})"
    )
    while pipeline.status not in ["manual", "running"]:
        time.sleep(CHECKER_SLEEP_TIME)
        pipeline = __get_pipeline(pipeline.id)
    UprevLogger().info(
        f"Triggering jobs for pipeline {pipeline.web_url} ({pipeline.status})"
    )
    __pipeline_trigger_jobs(pipeline, JobStageGroup.build)
    pipeline, jobs_stata = __wait_pipeline_start(pipeline)
    pipeline = __wait_pipeline_finished(pipeline, jobs_stata)
    return __post_process_pipeline(pipeline)


def __pipeline_trigger_jobs(
    pipeline: ProjectPipeline,
    classification: JobStageGroup,
) -> int:
    """
    Check if there are jobs in the pipeline that requires to be triggered.
    Depending on the project, the build jobs are grouped in some stages.
    :param pipeline: ProjectPipeline
    :param classification: information about which jobs should be triggered
    :return: number of triggered jobs
    """
    triggered = defaultdict(list)

    def __trigger_job(job: ProjectPipelineJob):
        if __pipeline_trigger_job_condition(job, classification):
            pjob = __get_job(job.id, lazy=True)
            pjob.play()
            UprevLogger().debug(f"Triggered {job.name} ({job.stage}) job")
            triggered[job.stage].append(job.name)

    with ThreadPoolExecutor(max_workers=16) as executor:
        t_0 = datetime.now()
        triggering = [
            executor.submit(__trigger_job, job)
            for job in pipeline.jobs.list(
                iterator=True, include_retried=False, scope="manual"
            )
        ]
        wait_threads_pool(triggering)
        UprevLogger().debug(
            f"Trigger threaded loop took {datetime.now() - t_0}"
        )

    if triggered:
        UprevLogger().info(f"Trigger jobs iteration: {dict(triggered)}")
    return len(triggered.items())


@retry(
    stop=stop_after_attempt(10),
    wait=wait_exponential(multiplier=1, max=180),
    after=after_log(UprevLogger().logger_obj, ERROR),
)
def __get_job(idn: int, lazy: bool = False) -> ProjectJob:
    working_project = Working().gl_project
    return working_project.jobs.get(idn, lazy=lazy)


def __pipeline_trigger_job_condition(
        job: ProjectPipelineJob,
        condition: JobStageGroup
) -> bool:
    """
    Determines whether a pipeline job should be triggered based on certain
    conditions.
    :param job: The pipeline job to evaluate.
    :param condition: condition based on the job classification.
    :return: Boolean indicating whether the job should be triggered.
    """
    match condition:
        case JobStageGroup.build:
            meets_condition = __is_build_job
        case JobStageGroup.test:
            meets_condition = __is_test_job
        case JobStageGroup.other:
            def meets_condition(_job):
                return not __is_build_job(_job) and not __is_test_job(_job)
    if job.status == "manual" and meets_condition(job):
        return True
    return False


def __post_process_pipeline(pipeline: ProjectPipeline) -> dict:
    """
    Check if there are results in the pipeline jobs. When some job has finished
    without artifacts, give it a second chance to make them.
    To detect flakes in the tests, the failed jobs are retried to see if they
    produce the same outcome.
    :param pipeline: pipeline to post process
    :return: dictionary with the failures found
    """
    failures, artifacts, wo_artifacts, stuck = collate_results(pipeline)
    if stuck:
        UprevLogger().warning(
            f"Some jobs failed because not DUTs available (stuck): "
            f"{', '.join([job.name for job in stuck])}"
        )
    if wo_artifacts:
        UprevLogger().warning(
            f"Some jobs failed without producing artifacts: "
            f"{', '.join([job.name for job in wo_artifacts])}"
        )
        pipeline, artifacts = __retry_jobs_without_artifacts(
            pipeline, artifacts, wo_artifacts
        )
    if artifacts:
        UprevLogger().info("Retry failed jobs looking for flakes.")
        exclude = [job.name for job in wo_artifacts + stuck]
        _, failures, _ = __retry_flake_candidates(
            pipeline, failures, artifacts, exclude,
        )

    return failures


def __wait_pipeline_start(
        pipeline: ProjectPipeline
) -> Tuple[ProjectPipeline, dict]:
    """
    Once a manual jobs are triggered in a pipeline, there is a (usually short
    but non-negligible) time.
    It return a refreshed pipeline object, dictionary of jobs with they status.
    :param pipeline: ProjectPipeline
    :return: ProjectPipeline, dict
    """
    UprevLogger().info(
        f"Wait for pipeline {pipeline.web_url} to start "
        f"(status={pipeline.status})"
    )
    jobs_stata = {}
    while True:
        _, pipeline = __refresh_pipeline_status(pipeline)
        jobs_stata, _ = __refresh_pipeline_job_status(pipeline, jobs_stata)
        n_triggered = __pipeline_trigger_jobs(pipeline, JobStageGroup.build)
        if pipeline.status in ["running", "failed"]:
            UprevLogger().info(
                f"Pipeline {pipeline.web_url} in {pipeline.status} status"
            )
            return pipeline, jobs_stata
        if n_triggered == 0 and pipeline.status == "manual":
            UprevLogger().info(
                f"Pipeline {pipeline.web_url} in {pipeline.status} status, "
                f"without build jobs to waiting for a trigger."
            )
            return pipeline, jobs_stata
        time.sleep(CHECKER_SLEEP_TIME)


def __wait_pipeline_finished(
    pipeline: ProjectPipeline, jobs_stata: dict
) -> ProjectPipeline:
    """
    Periodically check if the pipeline has finished.
    It received a pipeline object and a dictionary with the last status of the
    jobs in the pipeline. Job changing status is logged, and it returns a
    refreshed pipeline object.
    :param pipeline: ProjectPipeline
    :param jobs_stata
    :return: ProjectPipeline
    """
    if pipeline.status in ["failed"]:
        return pipeline
    UprevLogger().info(
        f"Wait for pipeline {pipeline.web_url} to finish "
        f"(status={pipeline.status})"
    )
    stata = {}
    minutes_threshold = 10
    while True:
        status_changed, pipeline = __refresh_pipeline_status(pipeline)
        jobs_stata, stata = __refresh_pipeline_job_status(
            pipeline, jobs_stata, stata
        )
        __pipeline_trigger_jobs(pipeline, JobStageGroup.build)
        __pipeline_trigger_jobs(pipeline, JobStageGroup.test)
        if status_changed and pipeline.status not in [
            "created",
            "waiting_for_resource",
            "preparing",
            "pending",
            "running",
        ]:
            UprevLogger().info(
                f"Pipeline {pipeline.web_url} finished "
                f"(status={pipeline.status})"
            )
            return pipeline
        time_in_current_status = datetime.utcnow() - datetime.strptime(
            pipeline.updated_at[:19], "%Y-%m-%dT%H:%M:%S"
        )
        if (time_in_current_status.total_seconds() // 60) > minutes_threshold:
            minutes_threshold += 10
            UprevLogger().info(
                f"Pipeline in '{pipeline.status}' "
                f"for {time_in_current_status} and continue."
            )
        time.sleep(CHECKER_SLEEP_TIME)


def __refresh_pipeline_status(
    pipeline: ProjectPipeline,
) -> Tuple[bool, ProjectPipeline]:
    """
    Check if the pipeline status has changed since the last object refresh.
    :param pipeline: ProjectPipeline
    :return: bool, ProjectPipeline
    """
    previous_pipeline_status = pipeline.status
    pipeline = __get_pipeline(pipeline.id)
    if previous_pipeline_status != pipeline.status:
        UprevLogger().debug(
            f"Pipeline {pipeline.id} status change: "
            f"{previous_pipeline_status} -> {pipeline.status}"
        )
        return True, pipeline
    return False, pipeline


def __refresh_pipeline_job_status(
    pipeline: ProjectPipeline, jobs_status: dict, stata: dict = None
) -> Tuple[dict, dict]:
    """
    It checks if there were any change in the state of the jobs of the
    pipeline.
    :param pipeline: ProjectPipeline
    :param jobs_status: dict
    :param stata: dict
    :return: (dict, dict)
    """
    new_stata = defaultdict(int)
    log_states = ["manual", "pending"]
    log_jobs = defaultdict(lambda: defaultdict(list))

    def __review_job(job: ProjectPipelineJob):
        new_stata[job.status] += 1
        if not __is_build_job(job) and not __is_test_job(job):
            return
        if job.status in log_states:
            log_jobs[job.status][job.stage].append(job.name)
        if job.name not in jobs_status.keys():
            UprevLogger().debug(
                f"Pipeline job '{job.name}' ('{job.stage}') "
                f"in '{job.status}' status."
            )
            jobs_status[job.name] = job.status
        elif job.status != jobs_status[job.name]:
            UprevLogger().debug(
                f"Pipeline job '{job.name}' ('{job.stage}') status change "
                f"from '{jobs_status[job.name]}' to '{job.status}'."
            )
            jobs_status[job.name] = job.status

    with ThreadPoolExecutor(max_workers=32) as executor:
        t_0 = datetime.now()
        bar = [
            executor.submit(__review_job, job)
            for job in pipeline.jobs.list(
                iterator=True, include_retried=False, sort_by="stage"
            )
        ]
        wait_threads_pool(bar)
        UprevLogger().debug(
            f"Refresh threaded loop took {datetime.now() - t_0}"
        )

    new_stata = dict(OrderedDict(sorted(new_stata.items())))
    if stata != new_stata:
        UprevLogger().info(f"Jobs stata summary {new_stata}")
        for job_status, jobs_dict in log_jobs.items():
            if (
                not isinstance(stata, dict) or
                job_status not in stata or
                job_status not in new_stata or
                stata[job_status] != new_stata[job_status]
            ):
                UprevLogger().warning(
                    f"Jobs in {job_status}: {dict(jobs_dict)}"
                )
    return jobs_status, dict(new_stata)


def __retry_jobs_without_artifacts(
    pipeline: ProjectPipeline, artifacts: dict, jobs_without_artifacts: list
) -> Tuple[ProjectPipeline, dict]:
    """
    if there are jobs without artifacts, lets give them another try
    :return:
    """
    UprevLogger().debug("Some jobs didn't produce artifacts")
    working_project = Working().gl_project
    for job in jobs_without_artifacts:
        UprevLogger().info(f"Retry '{job.name}' for artifacts build.")
        pjob = working_project.jobs.get(job.id, lazy=True)
        pjob.retry()
    pipeline, jobs_stata = __wait_pipeline_start(pipeline)
    pipeline = __wait_pipeline_finished(pipeline, jobs_stata)
    # collect results again, but:
    # - remembering artefacts already downloaded to avoid overwriting
    _, artifacts, jobs_without_artifacts, _ = collate_results(
        pipeline, artifacts=artifacts
    )
    if len(jobs_without_artifacts) != 0:
        job_names = [job.name for job in jobs_without_artifacts]
        __uprev_failed_issue(
            pipeline,
            f"These jobs failed to produce artifacts for "
            f"a second time: {', '.join(job_names)}",
        )
        sys.exit(0)
    return pipeline, artifacts


def __retry_flake_candidates(
    pipeline: ProjectPipeline, failures: dict, artifacts: dict, exclude: list,
) -> Tuple[ProjectPipeline, dict, dict]:
    """
    Do one retry in the jobs that may have failed to
    :param pipeline: ProjectPipeline
    :param failures: dictionary with already know failures per job
    :param artifacts: dictionary of the artifacts per job
    :param exclude: jobs to exclude in the retry
    :return: ProjectPipeline
    """
    working_project = Working().gl_project
    for job in pipeline.jobs.list(iterator=True):
        if job.status == "failed" and job.name not in exclude:
            UprevLogger().info(f"Retry '{job.name}' to discard flakes")
            pjob = working_project.jobs.get(job.id, lazy=True)
            pjob.retry()
    pipeline, jobs_stata = __wait_pipeline_start(pipeline)
    pipeline = __wait_pipeline_finished(pipeline, jobs_stata)
    # collect results again, but:
    # - remembering failures to collect together new results
    # - remembering artefacts already downloaded to avoid duplication
    _, _, jobs_without_artifacts, _ = collate_results(
        pipeline, failures=failures, artifacts=artifacts
    )
    if len(jobs_without_artifacts) != 0:
        pipeline, artifacts = __retry_jobs_without_artifacts(
            pipeline, artifacts, jobs_without_artifacts
        )
    failures, artifacts, _, _ = collate_results(
        pipeline, failures=failures, artifacts=artifacts, recollect=True
    )
    return pipeline, failures, artifacts


def __uprev_failed_issue(
    pipeline: ProjectPipeline,
    reason: str,
) -> None:
    """
    When the uprev produces jobs that doesn't produce artifacts, it means there
    is something deeper about the uprev that cannot be solved with an
    expectations update. Further information has to be provided to the
    developers to know about the situation.
    """
    job_id = os.getenv("CI_JOB_ID")
    if job_id is not None:
        gl = GitlabProxy().gl_obj
        uprev_project = gl.projects.get(os.getenv("CI_PROJECT_ID"))
        uprev_job = uprev_project.jobs.get(job_id)
        description = [
            f"This information comes from the execution of ci-uprev in job "
            f"[{uprev_job.id}]({uprev_job.web_url}).\n\n"
        ]
    else:
        description = [
            "This information comes from a local execution of ci-uprev.\n\n"
        ]
    description.append(
        f"Related pipeline: " f"[{pipeline.id}]({pipeline.web_url}).\n"
    )
    description.append(f"ci-uprev reported:\n\n\t{reason}\n")
    working_project = Working().gl_project
    uprev_commit = working_project.commits.get(pipeline.sha)
    start_commt = working_project.commits.get(uprev_commit.parent_ids[0])
    dep_project = Dependency().gl_project
    dep_commit = dep_project.commits.get(uprev_commit.title.split()[-1])
    target_project = get_target_project().gl_project
    description.append(
        f"This happened while doing an update revision of {dep_project.name} "
        f"[revision {dep_commit.short_id}]({dep_commit.web_url}) in "
        f"{target_project.name} [revision "
        f"{start_commt.short_id}]({start_commt.web_url}) with the uprev "
        f"[commit {uprev_commit.short_id}]({uprev_commit.web_url})."
    )
    __publish_uprev_issue("".join(description))


def __publish_uprev_issue(description: str) -> None:
    """
    When is necessary to publish some information in an issue in the target
    project, this method does that. But it only creates a new issue if there
    isn't one already opened. If there is already an issue, it appends a note.
    :param description: body text of the issue
    """
    issue = __search_existing_uprev_issue()
    issue_title = Title().for_issue
    target_project = get_target_project().gl_project
    if issue is None:
        labels = __get_uprev_labels()
        t_0 = datetime.now()
        issue = target_project.issues.create(
            {
                "title": issue_title,
                "description": description,
                "labels": labels
            }
        )
        UprevLogger().info(
            f"Created an issue with id {issue.iid}, "
            f"wait until the issue is ready to be used."
        )
        while _issue := __get_issue(issue.iid) is None:
            accumulated_wait_time = (datetime.now() - t_0).total_seconds()
            if accumulated_wait_time >= GL_OBJ_CREATION_TIMEOUT:
                raise RuntimeWarning(
                    f"Timeout reached while waiting for issue creation for "
                    f"id {issue.iid}"
                )
            time.sleep(CHECKER_SLEEP_TIME)
        UprevLogger().info(
            f"Created an issue with the runtime warning {issue.web_url}"
        )
        note = None
    else:
        note = issue.notes.create({"body": description})
        note_web_url = f"{issue.web_url}#note_{note.id}"
        UprevLogger().info(
            f"Append a note to an already existing issue with the runtime "
            f"warning {note_web_url}"
        )
    __link_issue_with_merge_request(issue, note)


def __search_existing_uprev_issue() -> Optional[ProjectIssue]:
    """
    Search in the target project if there is already an issue open about this
    uprev.
    :return: if exists, the issue, else None
    """
    target_project = get_target_project().gl_project
    query = {
        "state": "opened",
        "search": Title().for_issue,
        "in": "title",
        "order_by": "created_at",
    }
    try:
        issue = next(target_project.issues.list(
            query_parameters=query, iterator=True)
        )
        return target_project.issues.get(issue.iid)
    except StopIteration:
        return None


@retry(
    stop=stop_after_attempt(10),
    wait=wait_exponential(multiplier=1, max=180),
    after=after_log(UprevLogger().logger_obj, ERROR),
)
# retry waiting 1, 2, 4, 8, 16, 32, 64, 128, 180... > 10 minutes
def __get_issue(issue_iid: int) -> Optional[ProjectIssue]:
    """
    Get a issue object from the target project. If there is an exception, log
    it and reraise. It has also a decorator to use tenacity to retry.
    :param issue_iid: internal identifier number of the issue
    :return: object in gitlab representing an issue.
    """
    try:
        target_project = get_target_project().gl_project
        return target_project.issues.get(issue_iid)
    except GitlabGetError as exception:
        if exception.response_code == 404:
            return None
        UprevLogger().debug(
            f"Couldn't get the issue {issue_iid} "
            f"due to {exception.response_code}: {exception.response_body}"
        )
        raise exception
    except Exception as exception:
        UprevLogger().debug(
            f"Couldn't get the issue {issue_iid} "
            f"due to {type(exception)}: {exception}"
        )
        raise exception


def __get_uprev_labels() -> list:
    """
    Simple method where each target project can set up the labels issues or
    merge requests should have tagged with.
    :return: list of tags
    """
    uprev_pair = UpdateRevisionPair().enum
    if uprev_pair == UpdateRevision.mesa_in_virglrenderer:
        return ["ci"]
    elif uprev_pair == UpdateRevision.piglit_in_mesa:
        return []  # this is done by labelmaker
    else:
        return []


def __link_issue_with_merge_request(
    issue: ProjectIssue, note: ProjectNote = None
) -> None:
    mr_title = Title().for_merge_request
    merge_request = __get_uprev_merge_request(mr_title)
    if isinstance(merge_request, ProjectMergeRequest):
        description = ["It has been not possible to propose a newer uprev."]
        if not isinstance(note, ProjectNote):
            description.append(
                f"Further information on the issue " f"{issue.web_url}"
            )
        else:
            description.append(
                f"Further information on the issue note " f"{note.web_url}"
            )
        merge_request.notes.create({"body": "".join(description)})
    else:
        UprevLogger().debug(
            "There isn't a merge request in progress to report the creation "
            "of the issue"
        )


def collate_results(
    pipeline: ProjectPipeline,
    failures: dict = None,
    artifacts: dict = None,
    recollect: bool = False,
) -> Tuple[dict, dict, list, list]:
    """
    Find and collect the artifacts of the jobs. Classify them in a dictionary
    or, if there isn't, collect those jobs in a list.
    :param pipeline: ProjectPipeline
    :param failures: dict
    :param artifacts: dict
    :param recollect: bool
    :return: failures, artifacts, wo_artifacts, stuck
    """
    UprevLogger().info(
        f"Looking for failed jobs in pipeline {pipeline.web_url}"
    )
    if artifacts is None:
        artifacts = dict()
    if failures is None:
        # failures dict has as keys the tripplet (test_suite, backend, api)
        #  as value it has a nested dict where the key is the test_name and
        #  the values are deque of ens states of the tests in different reties.
        failures = defaultdict(defaultdictdeque)
    else:
        failures = deepcopy(failures)  # propagate will be decided by return
    wo_artifacts = []
    stuck = []

    working_project = Working().gl_project
    build_jobs_failed = []
    for job in pipeline.jobs.list(iterator=True):
        job = working_project.jobs.get(job.id)

        if job.status != "failed":
            continue

        if __is_build_job(job):
            # TODO: check if it has been retried
            build_jobs_failed.append(f"{job.name} ({job.stage})")
            UprevLogger().critical(
                f"Something very bad happened: "
                f"a build or sanity test job failed! "
                f"('{job.name}' job in '{job.stage}' stage)"
            )
            continue
        elif not __is_test_job(job):
            UprevLogger().warning(
                f"Something wrong with '{job.name}' (in {job.stage} stage), "
                f"but it's not affecting the uprev"
            )
            continue

        job_classification_key = __classify_the_job(job)

        if __download_artifacts(job, artifacts):
            results = artifacts[job.name]
        else:
            if len(job.trace()) == 0:
                stuck.append(job)
            else:
                wo_artifacts.append(job)
            continue

        __collate_failures(
            results, failures[job_classification_key], recollect
        )

    if build_jobs_failed:
        __uprev_failed_issue(
            pipeline,
            f"Something wrong with the build jobs: "
            f"{', '.join(build_jobs_failed)}",
        )
        sys.exit(0)

    if recollect:
        __implicit_results_become_explicit(failures)

    return failures, artifacts, wo_artifacts, stuck


def __is_build_job(job: ProjectJob) -> bool:
    """
    The jobs can be classified first in two main groups: jobs that provides
    build things to other jobs, and jobs that perform tests themselves.
    This method reports if the give job is classified as a build job for the
    uprev purposes.
    :param job: ProjectJob
    :return: bool
    """
    uprev_pair = UpdateRevisionPair().enum
    if uprev_pair == UpdateRevision.mesa_in_virglrenderer:
        return job.stage in ["build", "sanity test"]
    elif uprev_pair == UpdateRevision.piglit_in_mesa:
        # not all the build stages, but the ones we have interest
        return job.stage in [
            "sanity",
            "container",
            # "git-archive",
            "build-for-tests",
            # "build-only",
            "code-validation",
            # "deploy",
        ]


def __is_test_job(job: ProjectJob) -> bool:
    """
    The jobs can be classified first in two main groups: jobs that provides
    build things to other jobs, and jobs that perform tests themselves.
    This method reports if the give job is classified as a test job for the
    uprev purposes.
    :param job: ProjectJob
    :return: bool
    """
    uprev_pair = UpdateRevisionPair().enum
    if uprev_pair == UpdateRevision.mesa_in_virglrenderer:
        return job.stage in ["test"]
    elif uprev_pair == UpdateRevision.piglit_in_mesa:
        # Some jobs can be filtered by name
        if (re.match(r".*(traces|fossils|skqp|deqp|angle).*", job.name)
                and not re.match(f".*(piglit).*", job.name)):
            return False
        # TODO: perhaps reject also VK jobs.
        if re.match(r"(r300-).*", job.name) and job.stage == "amd-postmerge":
            return True
        # not all the testing stages, but the ones we have interest
        return job.stage in [
            "amd",
            # "amd-postmerge",
            "intel",
            # "intel-postmerge",
            "nouveau",
            # "nouveau-postmerge",
            "arm",
            # "arm-postmerge",
            "broadcom",
            # "broadcom-postmerge",
            "freedreno",
            # "freedreno-postmerge",
            "etnaviv",
            # "etnaviv-postmerge",
            "software-renderer",
            # "software-renderer-postmerge",
            "layered-backends",
            # "layered-backends-postmerge",
            # "performance",
        ]


def __classify_the_job(job: ProjectJob) -> Union[str, Tuple]:
    """
    Different uprev would have different ways to classify jobs. So we would
    specialize this method depending on the pair to uprev.
    :param job: ProjectJob
    :return: key
    """
    return __unshard_job_name(job.name)


def __download_artifacts(job: ProjectJob, artifacts: dict) -> bool:
    """
    For a given job, find if it has left artifacts (there could be more than
    one location to look at) to be downloaded. If so, collect it to be
    processed next. On the contrary, report returning false.
    :return:
    """
    try:
        if job.name not in artifacts:
            tmp = TemporaryDirectory()
            artifacts_zip_name = f"{tmp.name}/artifacts.zip"
            with open(artifacts_zip_name, "wb") as f:
                job.artifacts(streamed=True, action=f.write)
            UprevLogger().info(
                f"Downloaded artifacts of {job.name} at {artifacts_zip_name}"
            )
            artifacts_zip = ZipFile(artifacts_zip_name)
            artifact = b""
            artifact_found = False
            for element in artifacts_zip.filelist:
                if element.filename.endswith("failures.csv"):
                    UprevLogger().info(f"Artifact found: {element.filename}")
                    # Concatenate the content for all the failures.csv files
                    artifact += artifacts_zip.read(element.filename)
                    artifact_found = True
            if not artifact_found:
                raise FileNotFoundError("No artifacts found")
            artifacts[job.name] = artifact
        return True
    except Exception as exception:
        UprevLogger().error(
            f"An exception ({type(exception)}) occurred "
            f"when downloading results for '{job.name}' at {job.web_url}: "
            f"{exception}!"
        )
        return False


def __collate_failures(
        results: bytes,
        job_failures: dict,
        recollect: bool
) -> None:
    """
    Process the lines in the results downloaded to insert the data to the
    failures structure of a given job.
    :param results: content of the artifact downloaded
    :param job_failures: key-value of the test and the results it has had
    :param recollect: if this happened after a failed jobs retry
    :return:
    """
    for line in results.decode("utf-8").split("\n"):
        if not line.strip():
            continue
        test_name, result = line.split(",")
        # detect flakiness: collect the current execution results
        job_failures[test_name].append(result)
        if recollect and len(job_failures[test_name]) == 1:
            # As we only collect when fail is reported or unexpected pass,
            #  complete the information
            failures_item = job_failures[test_name]
            if result == "UnexpectedPass":
                failures_item.appendleft("ExpectedFail")
            else:
                failures_item.appendleft("Pass")


def __implicit_results_become_explicit(failures: dict) -> None:
    """
    review and complete the list of pairs with the implicit pass
    :param failures: dictionary with the results already collected
    :return: dictionary with the implicit results made explicit
    """
    for test_dict in failures.values():
        for results_deque in test_dict.values():
            if len(results_deque) == 1:
                if results_deque[0] == "UnexpectedPass":
                    results_deque.append("ExpectedFail")
                else:
                    results_deque.append("Pass")


def __unshard_job_name(name: str) -> str:
    """
    Remove the suffix on the sharded jobs to have a single name
    for all of them.
    :param name: string
    :return: original name before sharding
    """
    # if job is sharded, it has a " n/m" at the end, remove it.
    if search := re.match(r"(.*) [0-9]+/[0-9]+$", name):
        name = search.group(1)
    # remove the '-full' suffix
    if search := re.match(r"(.*)[-_]full$", name):
        name = search.group(1)
    # when after "-full", we have the arch tag
    elif search := re.match(r"(.*)[-_]full(:.*)$", name):
        name = f"{search.group(1)}{search.group(2)}"
    return name


def update_branch(
    repo: git.Repo,
    failures: dict,
    do_commit: bool = True,
) -> None:
    """
    Using the local clone of the repository, review the failures found
    (if there are any) to update the expectations (if required).
    :param repo: working repository
    :param failures: dictionary of the failures to update
    :param do_commit: flag to define if the clone should be modified
    :return:
    """
    os.chdir(repo.working_dir)
    try:
        index = repo.index  # This is expensive, reuse the index object
        update_expectations = False
        for job_key, test_dict in failures.items():
            expectations = __get_expectations_path_and_file(job_key)
            if expectations is None:
                continue
            fails, flakes, skips = __get_expectations_contents(*expectations)
            existing_fails = __fail_tests_and_their_causes(fails["content"])

            fixed_tests, update_expectations = __include_new_fails_and_flakes(
                job_key,
                test_dict,
                flakes,
                fails,
                skips,
                existing_fails,
            )

            __remove_fixed_tests(fixed_tests, fails["content"])

            for file_name, content in [
                (flakes["name"], flakes["content"]),
                (fails["name"], fails["content"]),
                (skips["name"], skips["content"]),
            ]:
                __patch_content(index, file_name, content, do_commit)

        if do_commit:
            if update_expectations:
                repo.git.commit("--amend", "--no-edit")
                UprevLogger().info(
                    f"Amended commit with new SHA {repo.head.commit.hexsha}\n"
                )
            __rename_active_branch(repo, Branch().for_merge_request)
    finally:
        os.chdir("..")


def __rename_active_branch(repo: git.Repo, new_name: str) -> None:
    """
    To complete the rename action, it is necessary to do it in two steps
    because the gitpython operation doesn't remove the old one in the remote.
    The new branch is only local now. The step to push it comes next by call
    push_to_merge_request().
    :param repo: working repository
    :param new_name: destination name for the temporary branch
    :return:
    """
    repo.active_branch.rename(new_name, force=True)
    try:
        # This branch is exceedingly rare in the remote, but it may happen if
        #  there is an interrupted execution of `ci-uprev` that pushes the
        #  branch and breaks before continuing the process.
        repo.remotes.fork.push(refspec=f":{Branch().for_wip}")
        UprevLogger().debug(
            f"Remove the {Branch().for_wip} in the remote fork project."
        )
    except git.exc.GitCommandError:
        UprevLogger().debug(
            f"The remote fork project doesn't have an "
            f"orphan {Branch().for_wip}"
        )


def __get_expectations_path_and_file(job_key: str) -> Optional[Tuple]:
    """
    Find in the structure where are located the expectation files for a given
    job. So it returns two strings. The first is with the path within the
    project. The second is with the prefix of the files that store the fails,
    flakes, and skips.
    :param job_key: string
    :return: a pair of strings
    """
    try:
        uprev_pair = UpdateRevisionPair().enum
        if uprev_pair == UpdateRevision.mesa_in_virglrenderer:
            expectations_paths = virglrenderer_expectations_paths
        elif uprev_pair == UpdateRevision.piglit_in_mesa:
            expectations_paths = mesa_expectations_paths
        else:
            target_project = Target().gl_project
            raise NotImplementedError(
                f"Not yet implemented the expectations "
                f"update for {target_project.name}"
            )
        path = expectations_paths[job_key]["path"]
        files = expectations_paths[job_key]["files"]
        if not path or not files:
            raise KeyError
        UprevLogger().debug(
            f"For {job_key!r} the path is {path!r} and files {files!r}"
        )
        return path, files
    except KeyError:
        UprevLogger().warning(
            f"The tool is not yet managing expectations for {job_key}"
        )
        return


def __get_expectations_contents(
        path: str,
        prefix: str
) -> Tuple[dict, dict, dict]:
    """
    Get the contents of the files that could be necessary to update the
    expectations.
    :param path: string
    :param prefix: string
    :return: three dicts with the name and content of the files
    """
    fails_file_name = f"{path}/{prefix}-fails.txt"
    fails = {
        "name": fails_file_name,
        "content": __get_file_content(fails_file_name)
    }
    flakes_file_name = f"{path}/{prefix}-flakes.txt"
    flakes = {
        "name": flakes_file_name,
        "content": __get_file_content(flakes_file_name)
    }
    skips_file_name = f"{path}/{prefix}-skips.txt"
    skips = {
        "name": skips_file_name,
        "content": __get_file_content(skips_file_name)
    }
    return fails, flakes, skips


def __fail_tests_and_their_causes(fails_contents: list) -> dict:
    """
    From the list of lines in the fails file, convert it to a dictionary where
    the key is the test name, and the value is the test results and the line.
    We can later check if the test has failed for the same reason or another.
    :param fails_contents:
    :return:
    """
    dct = {}
    for i, line in enumerate(fails_contents, start=1):
        if line.startswith("#") or line.count(",") == 0:
            continue  # ignore these lines
        test_name, test_result = line.strip().rsplit(",", 1)
        dct[test_name] = {"result": test_result, "line": i}
    return dct


def __include_new_fails_and_flakes(
    job_key: str,
    tests: dict,
    flakes: dict,
    fails: dict,
    skips: dict,
    existing_fails: dict,
) -> Tuple[list, bool]:
    """
    Prepare the content for the fails and flake files with the newer
    information from the uprev pipeline execution.
    :rtype: object
    :param job_key: identifier
    :param tests: list
    :param flakes: file name and content
    :param fails: file name and content
    :param skips: file name and content
    :param existing_fails: dict with the known fails and their result
    :return: list of test that are fix with the uprev, flag about if it has
             made an expectations update
    """
    expectations_change = False
    fixed_tests = []
    for test_name, test_results in tests.items():
        # remove duplicated elements
        test_results_unique = set(test_results)
        assert (
            len(test_results_unique) > 0
        ), "Failed, tests should have at least one result"
        if len(test_results_unique) != 1:
            UprevLogger().debug(
                f"Flaky test detected in {job_key} "
                f"{test_name=} with results {list(test_results)}"
            )
            flakes["content"] += [f"{test_name}\n"]
            expectations_change = True
        elif test_results[0] in ["UnexpectedPass",
                                 "UnexpectedImprovement(Pass)",
                                 "UnexpectedImprovement(Skip)",
                                 ]:
            UprevLogger().debug(
                f"{test_results[0]} test detected in {job_key} " f"{test_name=}"
            )
            fixed_tests += [test_name]
            expectations_change = True
        else:
            if test_results[0].startswith("UnexpectedImprovement"):
                # When there is an unexpected improvement,
                # use the value in the parenthesis to proceed.
                if match := re.match(
                        r"UnexpectedImprovement\((.*)\)", test_results[0]
                ):
                    test_results = [match.group(1)] * len(test_results)
            if test_results[0] in ["Timeout"]:
                file_name = skips["name"]
                content = skips["content"]
            else:
                file_name = fails["name"]
                content = fails["content"]
            if test_name in existing_fails.keys():
                previous_test_result = existing_fails[test_name]["result"]
                line = existing_fails[test_name]["line"]
                if test_results[0] == previous_test_result:
                    UprevLogger().debug(
                        f"Failed test detected in {job_key} {test_name=} "
                        f"with results '{test_results[0]}' was already in "
                        f"{file_name}:{line} with the same cause."
                    )
                    # This shouldn't happen, but log it just in case.
                else:
                    UprevLogger().debug(
                        f"Failed test detected in {job_key} {test_name=} "
                        f"with results '{test_results[0]}' was already in "
                        f"{file_name}:{line} with a different cause "
                        f"(was '{previous_test_result}')."
                    )
                    if test_name not in content:
                        content.append(f"{test_name}\n")
                    fixed_tests.append(test_name)
                    expectations_change = True
            else:
                UprevLogger().debug(
                    f"Failed test detected in {job_key} {test_name=} "
                    f"with results {test_results[0]}"
                )
                content += [f"{test_name},{test_results[0]}\n"]
                expectations_change = True
    return fixed_tests, expectations_change


def __remove_fixed_tests(fixed_tests: list, fails_content: list) -> None:
    """
    Review the list of tests that are fixed with the uprev to remove them from
    the list of tests that it was known they fail.
    :param fixed_tests:
    :param fails_content:
    :return:
    """
    for test_name in fixed_tests:
        found = False
        if f"{test_name},Fail\n" in fails_content:
            fails_content.remove(f"{test_name},Fail\n")
            found = True
        if f"{test_name},Crash\n" in fails_content:
            fails_content.remove(f"{test_name},Crash\n")
            found = True
        if found:
            UprevLogger().debug(
                f"Fixed test {test_name} removed from the fails file"
            )
        else:
            UprevLogger().debug(
                f"Fixed test {test_name} is not present in the fails file"
            )


def __patch_content(
        index: git.index.base.IndexFile,
        file_name: str,
        contents: list,
        add_index: bool
) -> None:
    """
    Once the file's content is ready, write in the file and add it to the git
    repo to prepare it to be committed.
    :param index: gitpython object to control the commit
    :param file_name: string
    :param contents: list of lines
    :param add_index: flag to include the change to a near commit
    :return:
    """
    with open(file_name, "wt") as file_descriptor:
        file_descriptor.writelines(contents)
    UprevLogger().info(f"Patched file {file_name}")
    if add_index:
        index.add(file_name)


def __get_file_content(file_name: str) -> list:
    """
    Return the lines in a file as a list.
    :param file_name:
    :return:
    """
    if os.path.exists(file_name):
        with open(file_name, "rt") as file_descriptor:
            return file_descriptor.readlines()
    else:
        return list()


def push_to_merge_request(
    repo: git.Repo, failures: dict, pipeline: ProjectPipeline
) -> None:
    """
    Once we have a viable candidate to uprev, generate or append to an
    existing merge request the proposal. It can be with or without an update
    on the expectations. Once the merge request has a pipeline to verify this
    proposal, the tool triggers it.
    :param repo: git.Repo
    :param failures: dict
    :param pipeline: ProjectPipeline
    :return: None
    """
    push_to_the_fork(repo, wait_pipeline_creation=False)
    regressions, flakes, unexpectedpass = __get_failures_by_category(failures)
    note_comments = __prepare_comments(
        pipeline, regressions, flakes, unexpectedpass
    )
    merge_request = __publish_uprev_merge_request()
    __append_note_to_merge_request(merge_request, note_comments)
    __run_merge_request_pipeline(merge_request, repo.head.commit.hexsha)


def __get_failures_by_category(failures: dict) -> Tuple[dict, dict, dict]:
    """
    From the collection of failures, classify them by category if there are
    regressions or flakes.
    :param failures: dict
    :return: Tuple[regressions, flakes, unexpectedpass]
    """
    regressions_per_category = defaultdict(dict)
    flake_per_category = defaultdict(dict)
    unexpectedpass_per_category = defaultdict(dict)
    for job_name, test_dict in failures.items():
        expectations = __get_expectations_path_and_file(job_name)
        if expectations is None:
            alert = " (Missing expectations update in the proposal)."
        else:
            alert = ""
        for test_name, results in test_dict.items():
            results_unique = set(results)
            if len(results_unique) != 1:
                category = f"Unreliable tests on {job_name}{alert}:\n"
                flake_per_category[category][test_name] = list(results)
            elif results[0] not in ["UnexpectedPass",
                                    "UnexpectedImprovement(Pass)"]:
                category = f"Possible regressions on {job_name}{alert}:\n"
                regressions_per_category[category][test_name] = results[0]
            else:
                assert len(results_unique) == 1
                assert results[0] in ["UnexpectedPass",
                                      "UnexpectedImprovement(Pass)"]
                category = f"Fix test with the uprev on {job_name}{alert}:\n"
                unexpectedpass_per_category[category][test_name] = results[0]
    return (regressions_per_category, flake_per_category,
            unexpectedpass_per_category)


def __prepare_comments(
    pipeline: ProjectPipeline,
    regressions_per_category: dict,
    flake_per_category: dict,
    unexpectedpass_per_category: dict,
) -> Tuple[list, list, list, list]:
    """
    With the information available, prepare the list of comments to be added
    in a note in the merge request.
    :param pipeline: ProjectPipeline
    :param regressions_per_category: dict
    :param flake_per_category: dict
    :param unexpectedpass_per_category: dict
    :return: list
    """
    comments = (
        __source_comment(pipeline),
        __generate_comment(regressions_per_category, False),
        __generate_comment(flake_per_category, True),
        __generate_comment(unexpectedpass_per_category, False),
    )
    return comments


def __publish_uprev_merge_request() -> ProjectMergeRequest:
    """
    Check if a merge request is already in progress or create a newer one.
    :return: ProjectMergeRequest
    """
    fork_project = Fork().gl_project
    uprev_branch = Branch().for_merge_request
    target_project = get_target_project().gl_project
    mr_title = Title().for_merge_request
    merge_request = __get_uprev_merge_request(mr_title)

    if merge_request is None:
        labels = __get_uprev_labels()
        merge_request = fork_project.mergerequests.create(
            {
                "source_branch": uprev_branch,
                "target_branch": target_project.default_branch,
                "target_project_id": target_project.id,
                "title": mr_title,
                "labels": labels,
            }
        )
        UprevLogger().info(f"Created merge request {merge_request.web_url}")
    else:
        UprevLogger().info(
            f"Pushed to existing merge request {merge_request.web_url}"
        )
    __link_merge_request_with_issue(merge_request)
    return merge_request


@cache
@retry(
    stop=stop_after_attempt(10),
    wait=wait_exponential(multiplier=1, max=180),
    after=after_log(UprevLogger().logger_obj, ERROR),
)
# retry waiting 1, 2, 4, 8, 16, 32, 64, 128, 180... > 10 minutes
def __get_uprev_merge_request(title: str) -> Optional[ProjectMergeRequest]:
    username = GitlabProxy().gl_obj.user.username
    uprev_branch = Branch().for_merge_request
    target_project = get_target_project().gl_project
    query = {
        "state": "opened",
        "author_username": username,
        "search": title,
        "source_branch": uprev_branch,
    }
    try:
        mr = next(
            target_project.mergerequests.list(
                query_parameters=query, iterator=True
            )
        )
        return target_project.mergerequests.get(mr.iid)
    except StopIteration:
        # to scape tenacity when the exception is because there isn't such MR.
        return None


def __link_merge_request_with_issue(
        merge_request: ProjectMergeRequest
) -> None:
    issue = __search_existing_uprev_issue()
    if isinstance(issue, ProjectIssue):
        description = [
            f"Uprev candidate generated in merge request "
            f"{merge_request.web_url}. The issue is solved."
        ]
        issue.notes.create({"body": "".join(description)})
        if in_production():
            try:
                issue.state_event = "close"
                issue.save()
            except GitlabUpdateError as exception:
                UprevLogger().error(
                    f"Exception '{exception.error_message}' when closing the "
                    f"issue about previous problems to uprev. "
                    f"See {issue.web_url}"
                )
    else:
        UprevLogger().debug(
            "There isn't a previously in progress issue about this uprev to "
            "report this new uprev candidate."
        )


def __append_note_to_merge_request(
    merge_request: ProjectMergeRequest, comments: Tuple[list, list, list, list]
) -> None:
    """
    With new or in-progress merge requests, add a note with information about
    the current proposal to uprev.
    :param merge_request: ProjectMergeRequest
    :param comments: list
    :return:
    """
    comments = ["".join(c) for c in comments if c]
    merge_request.notes.create({"body": "\n".join(comments)})


def __run_merge_request_pipeline(
    merge_request: ProjectMergeRequest, commit_hash: str
) -> None:
    """
    Once the merge request has the information about the uprev proposal, a
    pipeline is created to verify that the proposal doesn't brake previous
    work. Creating, triggering, and monitoring the evolution is the last part
    of the tasks of this tool.
    :param merge_request: ProjectMergeRequest
    :return: None
    """
    merge_request = __wait_until_merge_request_commit_matches(
        merge_request, commit_hash
    )
    # Depending on the groups the user belongs, this merge request could have
    # the pipeline in the fork or in the main project. So, we must check where
    # the information we have points to continue the process.
    merge_request = __wait_commits_merge_request_head_pipeline(
        merge_request, commit_hash
    )
    merge_request = __check_merge_request_source(merge_request)
    __check_merge_request_pipeline_project(merge_request)
    merge_request_pipeline = __wait_pipeline_creation(
        commit_hash, source="merge_request_event"
    )
    failures = run_pipeline(merge_request_pipeline)
    if merge_request_pipeline.status == "failed" or "failed" in {
        job.status
        for job in merge_request_pipeline.jobs.list(
            iterator=True, include_retried=False
        )
    }:
        # Check if the status is failed. In some cases we may see manual but
        # some job in failed status. So first do the quick check because the
        # second could be avoided if the first is already true. It is the
        # default, but explicitly exclude the retried, because the interest is
        # in the final result.
        msg = (
            f"Something wrong in the verification pipeline "
            f"{merge_request_pipeline.web_url}"
        )
        if failures:
            UprevLogger().critical(f"{msg}\n{pformat(failures)}")
        raise RuntimeError(msg)
    elif failures:
        UprevLogger().warning(
            f"Flaky verification pipeline\n{pformat(failures)}"
        )


def __wait_until_merge_request_commit_matches(
    merge_request: ProjectMergeRequest, commit_hash: str
) -> ProjectMergeRequest:
    """
    This method will wait until the merge request points to the expected
    commit hash.
    :param merge_request: merge request where the commit was pushed
    :param commit_hash: sha1 of the commit previously pushed
    :return: refreshed merge request object
    """
    first_attempt = True
    while True:
        if merge_request.sha == commit_hash:
            UprevLogger().info(
                f"Merge request with latest commit {merge_request.sha}. "
                f"Look for its merge request pipeline!"
            )
            return merge_request
        if first_attempt:
            UprevLogger().info(
                "Merge request doesn't have yet the latest commit. Wait..."
            )
            first_attempt = False
        time.sleep(CHECKER_SLEEP_TIME)
        merge_request = __get_merge_request(merge_request.iid)


def __wait_commits_merge_request_head_pipeline(
    merge_request: ProjectMergeRequest, commit_hash: str
) -> ProjectMergeRequest:
    """
    This method will wait until the head pipeline of the merge request points
    to the expected commit hash.
    :param merge_request: merge request where the commit was pushed
    :param commit_hash: sha1 of the commit previously pushed
    :return: refreshed merge request object
    """
    first_attempt_of_none = True
    first_attempt_of_sha = True
    while True:
        if merge_request.head_pipeline is not None:
            if merge_request.head_pipeline["sha"] == commit_hash:
                UprevLogger().info(
                    f"Merge request head pipeline "
                    f"{merge_request.head_pipeline['id']} "
                    f"points to the latest commit "
                    f"{merge_request.head_pipeline['sha']}."
                )
                return merge_request
            if first_attempt_of_sha:
                UprevLogger().info(
                    f"Merge request head pipeline "
                    f"{merge_request.head_pipeline['id']} "
                    f"doesn't point yet to the latest commit. Wait..."
                )
                first_attempt_of_sha = False
        elif first_attempt_of_none:
            UprevLogger().info(
                "Merge request doesn't have yet a head pipeline. Wait..."
            )
            first_attempt_of_none = False
        time.sleep(CHECKER_SLEEP_TIME)
        merge_request = __get_merge_request(merge_request.iid)


def __check_merge_request_source(
    merge_request: ProjectMergeRequest,
) -> ProjectMergeRequest:
    """
    This method will wait until the head pipeline of the merge request
    corresponds to one from the merge request event.
    :param merge_request: merge request where the uprev was pushed
    :return: refreshed merge request object
    """
    first_attempt = True
    while True:
        if merge_request.head_pipeline["source"] == "merge_request_event":
            UprevLogger().info(
                f"Head pipeline {merge_request.head_pipeline['id']} is from "
                f"a merge request event "
                f"({merge_request.head_pipeline['source']})."
            )
            return merge_request
        if first_attempt:
            UprevLogger().info(
                f"Head pipeline {merge_request.head_pipeline['id']} doesn't "
                f"point yet to merge request event. Wait..."
            )
            first_attempt = False
        time.sleep(CHECKER_SLEEP_TIME)
        merge_request = __get_merge_request(merge_request.iid)


def __check_merge_request_pipeline_project(
        merge_request: ProjectMergeRequest
) -> None:
    """
    Depending on the rights of the user running the uprev, the merge request
    pipeline is created in the main project or in the fork. So, this method
    checks in which of them is created and configures the tool to proceed
    accordingly.
    :param merge_request: merge request where the uprev was pushed
    :return:
    """
    project_id = merge_request.head_pipeline["project_id"]
    project = GitlabProxy().gl_obj.projects.get(project_id)
    UprevLogger().info(
        f"Merge request with the pipeline {merge_request.head_pipeline['id']} "
        f"from the latest commit (in {project.path_with_namespace}). "
        f"Run this pipeline! {merge_request.head_pipeline['web_url']}"
    )
    if Target().gl_project == project:
        Working().project = Target()
    elif Fork().gl_project == project:
        Working().project = Fork()
    else:
        raise RuntimeError(
            f"The merge request pipeline is in project "
            f"{project.path_with_namespace}, not the Target or the Fork."
        )


@retry(
    stop=stop_after_attempt(10),
    wait=wait_exponential(multiplier=1, max=180),
    after=after_log(UprevLogger().logger_obj, ERROR),
)
def __get_merge_request(mergerequest_id: int) -> ProjectMergeRequest:
    """
    Get a merge request object from the target project. If there is an
    exception, use tenacity to retry.
    A practical use of this method is to refresh the gitlab object.
    :param mergerequest_id: merge request identifier
    :return:
    """
    return get_target_project().gl_project.mergerequests.get(mergerequest_id)


def __source_comment(pipeline: ProjectPipeline) -> list:
    """
    With environment information, prepare a few lines for the merge request
    note reporting how this proposal has been produced.
    :param pipeline: ProjectPipeline
    :return: lines as list
    """
    job_id = os.getenv("CI_JOB_ID")
    if job_id is not None:
        gl = GitlabProxy().gl_obj
        uprev_project = gl.projects.get(os.getenv("CI_PROJECT_ID"))
        uprev_job = uprev_project.jobs.get(job_id)
        source_comment = [
            f"This information comes from the execution of ci-uprev in job "
            f"[{uprev_job.id}]({uprev_job.web_url}).\n"
        ]
    else:
        source_comment = [
            "This information comes from a local execution of ci-uprev.\n"
        ]
    pipelines_summary = f"Related pipeline: " \
                        f"[{pipeline.id}]({pipeline.web_url})"
    pipelines_summary += ".\n"
    source_comment += [pipelines_summary]
    dependency_diff = Dependency().diff
    if dependency_diff:
        source_comment.append(
            f"The dependency changes can be reviewed with "
            f"this [compare]({dependency_diff}).\n"
        )
    return source_comment


def __generate_comment(by_category: dict, as_list: bool) -> list:
    """
    Transform the dictionary with the information by category to the format we
    place this report in the merge request note.
    :param by_category: dict
    :param as_list: bool
    :return: lines as list
    """
    comment = []
    for category, problem in by_category.items():
        category_comment = [category]
        for test_name, result in problem.items():
            if as_list:
                line = f"- {test_name}, {list(result)}\n"
            else:
                line = f"- {test_name}, {result}\n"
            category_comment.append(line)
        category_comment = __truncate_long_messages(category_comment)
        category_comment.append("\n")
        comment.extend(category_comment)
    return comment


def __truncate_long_messages(messages: list[str]) -> list[str]:
    """
    When a message list of lines is too long and could disturb the reading of
    the user, this method packs the lines over a limit in a details Markdown
    code.
    :param messages: lines in a message
    :return: original or, if needed, truncated message
    """
    if len(messages) > LINES_PER_CATEGORY_TO_SUMMARY_IN_NOTE:
        messages, details, excedent = (
            messages[:LINES_PER_CATEGORY_TO_SUMMARY_IN_NOTE],
            messages[
                LINES_PER_CATEGORY_TO_SUMMARY_IN_NOTE:
                MAX_LINES_PER_CATEGORY_IN_NOTE
            ],
            messages[MAX_LINES_PER_CATEGORY_IN_NOTE:],
        )
        messages.append("\n\n<details>\n")
        messages.append("\n<summary>... and more</summary>\n\n")
        messages.extend(details)
        if excedent:
            # TODO: link to the commit content
            messages.append(
                "\nToo much, to see more check the file in the commit\n"
            )
        messages.append("\n</details>\n")
    return messages


def main():
    args = __cli_arguments()

    try:
        __cmd_uprev(args.revision, args.pipeline)
    except NothingToUprev:
        UprevLogger().info(
            "Latest commit in the dependency project is the revision "
            "already in use in the target project. No need for uprev."
        )
        return


def __cli_arguments() -> Namespace:
    """
    Define the command line behavior for the arguments and subcommands.
    :return: argument parser
    """
    parser = ArgumentParser(
        description="CLI tool to update the revision of a project used "
        "inside another project."
    )
    # TODO: mention that no argument is required
    #  all the configuration elements come from environment variables and this
    #  arguments are only for development and debugging.
    #  So, also show help about those envvars
    #   TARGET_PROJECT_PATH, FORK_PROJECT_PATH, DEP_PROJECT_PATH, GITLAB_TOKEN
    parser.add_argument(
        "--revision", metavar="HASH",
        help="Specify the revision to uprev."
    )
    parser.add_argument(
        "--pipeline",
        metavar="PIPELINE_ID",
        help="Specify the pipeline to reference in the uprev. "
        "Requires to have specified the revision.",
    )
    return parser.parse_args()


def __cmd_uprev(
    revision: str,
    pipeline_id: int,
) -> None:
    """
    This is the default procedure of the uprev tool. Without options, the
    default, it will search for a pipeline in the project to uprev that
    satisfies the necessary conditions. The commit of this pipeline will be
    used to uprev in the target project. (Those two variables, revision and
    pipeline, can be fixed using option arguments for debug purposes like
    repeat and compare with a previous execution.)

    Then the uprev candidate is tested, if there are expectations to update
    they are and the commit amended, to then generate a merge request proposal.

    :param revision: optional parameter to specify the revision to use
    :param pipeline_id: optional parameter to specify the pipeline to use
    :return:
    """
    dep_project = Dependency().gl_project
    if not revision and not pipeline_id:
        pipeline_id, revision = get_candidate_for_uprev()
        UprevLogger().info(
            f"Found pipeline {pipeline_id} with SHA {revision} "
            f"{dep_project.commits.get(revision).web_url}"
        )
    else:
        if revision:
            pipeline_id = __search_pipeline(revision)
            UprevLogger().info(
                f"Specified the revision to uprev to {revision} and found "
                f"the pipeline {pipeline_id} "
                f"{dep_project.commits.get(revision).web_url}"
            )
        elif pipeline_id:
            revision = __get_pipeline_commit(pipeline_id)
            UprevLogger().info(
                f"Specified the pipeline to uprev to {pipeline_id} and found "
                f"the revision {revision} "
                f"{dep_project.commits.get(revision).web_url}"
            )
    uprev_pair = UpdateRevisionPair().enum
    if uprev_pair == UpdateRevision.mesa_in_virglrenderer:
        templates_commit = get_templates_commit(revision)
    elif uprev_pair == UpdateRevision.piglit_in_mesa:
        templates_commit = get_templates_commit("main")
    else:
        raise NotImplementedError(
            f"Not yet implemented the uprev of " f"{uprev_pair.name}"
        )
    repo = create_branch(pipeline_id, revision, templates_commit)
    pipeline = push_to_the_fork(repo)
    failures = run_pipeline(pipeline)
    update_branch(repo, failures)
    push_to_merge_request(repo, failures, pipeline)


def __search_pipeline(revision: str) -> int:
    """
    When a revision is specified, the pipeline it has generated is needed.
    :param revision: dep project revision
    :return: pipeline id
    """
    dep_project = Dependency().gl_project
    pipelines = dep_project.pipelines.list(sha=revision, state="success")
    if not pipelines:
        raise ValueError("No pipeline found with this revision")
    return pipelines[-1].id


def __get_pipeline_commit(pipeline_id: int) -> str:
    """
    When a pipeline is specified, the revision that generated it is needed.
    :param pipeline_id: identification number
    :return: string with the commit sha
    """
    pipeline = __get_pipeline(pipeline_id, project=Dependency().gl_project)
    return pipeline.sha


if __name__ == "__main__":
    main()

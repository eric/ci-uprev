#!/usr/bin/env python3.11

# Copyright (C) 2023-2024 Collabora Ltd
#
# Permission is hereby granted, free of charge, to any person obtaining a
# copy of this software and associated documentation files (the "Software"),
# to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense,
# and/or sell copies of the Software, and to permit persons to whom the
# Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included
# in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
# OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
# THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR
# OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
# ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
# OTHER DEALINGS IN THE SOFTWARE.
#
# SPDX-License-Identifier: MIT

__author__ = "Sergi Blanch Torne"
__email__ = "sergi.blanch.torne@collabora.com"
__copyright__ = "Copyright (C) 2023-2024 Collabora Ltd"


from gitlab import Gitlab
from gitlab.v4.objects import Project
import os
from typing import Union

from .abstract import Singleton
from .uprevs import UpdateRevision


_gl_fdo_url: str = "https://gitlab.freedesktop.org"


def _gitlab_builder(url, private_token=None):
    return Gitlab(
        url,
        private_token=private_token,
        retry_transient_errors=True,
    )


class GitlabProxy(Singleton):
    __gl: Gitlab = None
    __token: str = None

    def __init__(self):
        self.__get_gitlab_token()
        self.__get_gitlab_object()

    @property
    def token(self):
        return self.__token

    @property
    def gl_obj(self):
        return self.__gl

    def __get_gitlab_token(self) -> None:
        self.__token = os.environ.get("GITLAB_TOKEN")
        if self.__token is None:
            try:
                with open(os.path.expanduser("~/.gitlab-token")) as f_descr:
                    self.__token = f_descr.read().strip()
            except FileNotFoundError:
                raise AssertionError(
                    "Use the GITLAB_TOKEN environment variable or "
                    "the ~/.gitlab-token to provide this information"
                )

    def __get_gitlab_object(self) -> None:
        self.__gl = _gitlab_builder(
            url=_gl_fdo_url,
            private_token=self.__token
        )
        self.__gl.auth()


class _UprevProject(Singleton):
    _envvar: str = None
    _full_name: str = None
    _namespace: str = None
    _path: str = None
    _project: Project = None

    def __init__(self):
        self._full_name = self.get_envvar_value()
        self._namespace, self._path = self._full_name.rsplit("/", 1)
        self._project = GitlabProxy().gl_obj.projects.get(self._full_name)

    @property
    def path_with_namespace(self) -> str:
        return self._full_name

    @property
    def namespace(self) -> str:
        return self._namespace

    @property
    def path(self) -> str:
        return self._path

    @property
    def gl_project(self) -> Project:
        return self._project

    def get_envvar_value(self) -> str:
        envvar_value = os.environ.get(self._envvar)
        if envvar_value is None:
            raise AssertionError(
                f"It is required to define the "
                f"environment variable {self._envvar}."
            )
        return envvar_value


class Target(_UprevProject):
    def __init__(self):
        self._envvar = "TARGET_PROJECT_PATH"
        super(Target, self).__init__()


class Fork(_UprevProject):
    def __init__(self):
        self._envvar = "FORK_PROJECT_PATH"
        try:
            super(Fork, self).__init__()
        except AssertionError:
            # When it is not defined, create it pointing it to
            # the user running the tool fork.
            self._namespace = GitlabProxy().gl_obj.user.username
            self._path = Target().path
            self._full_name = f"{self._namespace}/{self._path}"
            self._project = GitlabProxy().gl_obj.projects.get(self._full_name)


def get_target_project() -> "_UprevProject":
    """
    Factory function to create an instance of either `Target` or `Fork` based
    on the runtime environment.

    The decision to instantiate `Target` or `Fork` is determined by the value
    of the "PRODUCTION" environment variable. If the environment is set to
    production (i.e., "PRODUCTION" is set to "true" or "1"), an instance of
    `Target` is returned. Otherwise, an instance of `Fork` is created.

    Note:
    - If the "PRODUCTION" environment variable is set to an unrecognized value,
      an assertion error is raised.
    - If the `Fork` class fails to initialize due to a missing project path,
      it attempts to derive it from the user's GitLab username and the path of
      the `Target` project.

    Returns:
        _UprevProject: An instance of either `Target` or `Fork` depending on
        the runtime environment.

    Raises:
        AssertionError: If the "PRODUCTION" environment variable contains an
        unrecognized value.
    """
    if in_production():
        return Target()
    return Fork()


class Dependency(_UprevProject):
    __diff: str = None

    def __init__(self):
        self._envvar = "DEP_PROJECT_PATH"
        super(Dependency, self).__init__()

    @property
    def diff(self) -> str:
        return self.__diff

    @diff.setter
    def diff(self, diff: str) -> None:
        self.__diff = diff


class Working(Singleton):
    __working_project: Union[Target, Fork] = None

    @property
    def project(self) -> Union[Target, Fork]:
        return self.__working_project

    @project.setter
    def project(self, project: Union[Target, Fork]) -> None:
        self.__working_project = project

    @property
    def gl_project(self):
        return self.__working_project.gl_project


class Template(Singleton):
    __template_project: Union[Target, Dependency] = None

    @property
    def project(self) -> Union[Target, Dependency]:
        return self.__template_project

    @project.setter
    def project(self, project: Union[Target, Dependency]) -> None:
        self.__template_project = project

    @property
    def gl_project(self):
        return self.__template_project.gl_project


class UpdateRevisionPair(Singleton):
    __name: str = None
    __target_name: str = None
    __dependency_name: str = None
    __uprev_pair: UpdateRevision = None

    def __init__(self):
        self.__target_name = Target().path
        self.__dependency_name = Dependency().path
        try:
            self.__name = f"{self.__dependency_name}_in_{self.__target_name}"
            self.__uprev_pair = UpdateRevision[self.__name]
            template = Template()
            if self.__uprev_pair == UpdateRevision.mesa_in_virglrenderer:
                template.project = Dependency()
            elif self.__uprev_pair == UpdateRevision.piglit_in_mesa:
                template.project = Target()
        except AttributeError:
            raise NotImplementedError(
                f"Not yet implemented the uprev of "
                f"{self.__dependency_name} in {self.__target_name}"
            )

    @property
    def enum(self) -> UpdateRevision:
        return self.__uprev_pair


class LocalClone(Singleton):
    __directory: str = None

    def __init__(self):
        self.__directory = f"./tmp_{Target().path}.git"

    @property
    def directory(self) -> str:
        return self.__directory


class _InProduction(Singleton):
    __in_production: bool = None

    def __init__(self):
        production_envvar = os.environ.get("PRODUCTION", "False").lower()
        if production_envvar not in ["0", "1", "false", "true"]:
            raise AssertionError(
                f"Unrecognized value in PRODUCTION environment variable "
                f"(value '{production_envvar}')"
            )
        self.__in_production = production_envvar in ("1", "true")

    @property
    def value(self) -> bool:
        return self.__in_production


def in_production() -> bool:
    return _InProduction().value


class Branch(Singleton):
    __wip_branch: str = None
    __merge_request_branch: str = None

    def __init__(self):
        dep_name = Dependency().path
        # Branch to which we push if there are only UnexpectedPasses
        self.__merge_request_branch = f"uprev-{dep_name}"
        if not in_production():
            self.__merge_request_branch = \
                f"{self.__merge_request_branch}-testonly"
        # Branch to which we first push to get the initial results
        self.__wip_branch = f"{self.__merge_request_branch}-tmp"

    @property
    def for_merge_request(self) -> str:
        return self.__merge_request_branch

    @property
    def for_wip(self) -> str:
        return self.__wip_branch


class Title(Singleton):
    __merge_request_title: str = None
    __issue_title: str = None

    def __init__(self):
        dep_name = Dependency().path
        self.__merge_request_title = f"ci: Uprev {dep_name.title()}"
        self.__issue_title = f"{self.__merge_request_title} failed"
        if not in_production():
            self.__merge_request_title = (
                f"Draft: [TESTONLY] {self.__merge_request_title}"
            )

    @property
    def for_merge_request(self) -> str:
        return self.__merge_request_title

    @property
    def for_issue(self) -> str:
        return self.__issue_title

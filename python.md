# Python environment

## Python using virtualenv

Based on python 3.11, one can setup a virtual environments. 

```bash
$ pip3 -V
pip 23.0.1 from /usr/lib/python3/dist-packages/pip (python 3.11)
```

A virtual environment can be created in order to use this module: 

```bash
$ python3 -m venv ci-uprev.venv
$ source ci-uprev.venv/bin/activate
```

To reproduce the same virtual environment that this tools is being tested:

```commandline
$ pip install -r requirements.txt
```

To update the packages in the requirements:

```commandline
$ pip install --upgrade pip
$ python -m pip install colorlog python-gitlab gitpython ruamel.yaml tenacity
$ pip list
Package            Version
------------------ ---------
certifi            2024.8.30
charset-normalizer 3.4.0
colorlog           6.9.0
gitdb              4.0.11
GitPython          3.1.43
idna               3.10
pip                24.3.1
python-gitlab      5.0.0
requests           2.32.3
requests-toolbelt  1.0.0
ruamel.yaml        0.18.6
ruamel.yaml.clib   0.2.12
setuptools         66.1.1
smmap              5.0.1
tenacity           9.0.0
urllib3            2.2.3
$ pip freeze --all > requirements.txt
```

### running tests

To reproduce the same virtual environment that this tools is being tested:

```bash
$ pip install -r requirements-tests.txt
```

or to the previous `requirements.txt` append:

```commandline
$ python -m pip install pytest
$ pip install git+https://gitlab.freedesktop.org/sergi/python-gitlab-mock
$ pip install -e .
```

One can use a local repo of `python-gitlab-mock` but its development is deprecated,
and we must migrate this to the same testing pattern than `python-gitlab`.

At this point, one can call pytest:

```commandline
$ pytest
```


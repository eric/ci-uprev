#!/usr/bin/env python3.9

# Copyright (C) 2023 Collabora Ltd
#
# Permission is hereby granted, free of charge, to any person obtaining a
# copy of this software and associated documentation files (the "Software"),
# to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense,
# and/or sell copies of the Software, and to permit persons to whom the
# Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included
# in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
# OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
# THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR
# OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
# ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
# OTHER DEALINGS IN THE SOFTWARE.
#
# SPDX-License-Identifier: MIT

__author__ = "Sergi Blanch Torne"
__email__ = "sergi.blanch.torne@collabora.com"
__copyright__ = "Copyright (C) 2023 Collabora Ltd"
__license__ = "MIT"

__project__ = "uprev"
__description__ = (
    "Python module with entrypoints to review continuous "
    "integration systems to move update revision by forward "
    "the dependencies of a given project."
)
__long_description__ = """
This module has been developed to provide tools to simplify and improve the
process to check if there are newer candidate revision of project 
dependencies to be used.

This code is the natural evolution of the initial idea of uprev the mesa used
in virglrenderer.

See more details in the readmes of the source repository.
"""
__url__ = "https://gitlab.freedesktop.org/gfx-ci/ci-uprev"
__version__ = "0.2.2"


from setuptools import setup, find_packages


classifiers = [
    "Development Status :: 2 - Pre-Alpha",
    "Intended Audience :: Developers",
    "License :: OSI Approved :: MIT License",
    "Operating System :: POSIX",
    "Programming Language :: Python :: 3.9",
    "Programming Language :: Python :: 3.10",
    "Topic :: Software Development :: Libraries :: Python Modules",
]


setup(
    name=__project__,
    license=__license__,
    description=__description__,
    long_description=__long_description__,
    version=__version__,
    author=__author__,
    author_email=__email__,
    classifiers=classifiers,
    packages=find_packages(),
    package_data={"uprev.expectations": ["*.yml"]},
    url=__url__,
    entry_points={"console_scripts": ["ci-uprev=uprev:main"]},
)

# for the classifiers review see:
# https://pypi.python.org/pypi?%3Aaction=list_classifiers
#
# Development Status :: 1 - Planning
# Development Status :: 2 - Pre-Alpha
# Development Status :: 3 - Alpha
# Development Status :: 4 - Beta
# Development Status :: 5 - Production/Stable
